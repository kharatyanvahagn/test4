﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Student
    {
        public string Name { get; set; }
        public string Degree { get; set; }

    }
    abstract class EducationalInstitution
    {
        public string name, address;

        //public int age;
        public abstract string Name
        {
            set;
        }
        public abstract string Address
        {
            set;
        }
        public abstract void Display();
    }
    class College : EducationalInstitution
    {

        public int age;

        public int Age
        {
            set { this.age = value; }
        }

        public override string Name
        {
            set { this.name = value; }

        }
        public override string Address
        {
            set { this.address = value; }
        }
        public override void Display()
        {
            Console.WriteLine("College Name is " + this.name);
            Console.WriteLine("College Address is " + this.address);
            Console.WriteLine("College Age is " + this.age);
        }

        class University : College
        {
            public List<string> degree = new List<string>() { " master's degree", " doctor degree" };
            public List<Student> students = new List<Student>();
            public override void Display()
            {
                Console.WriteLine("University Name is " + this.name);
                Console.WriteLine("University Address is " + this.address);
                Console.WriteLine("University Age is " + this.age);
            }
        }


        class Program
        {

            static void Main(string[] args)
            {
                College college = new College();
                college.Age = 50;
                college.Address = "addres1";
                college.Name = "name1";
                college.Display();

                University university = new University();
                university.Age = 60;
                university.Address = "addres2";
                university.Name = "name2";
                university.students.Add(new Student { Name = "StudntName", Degree = university.degree[0] });
                Console.WriteLine(university.students[0].Degree);
                university.Display();

            }
        }
    }
}

